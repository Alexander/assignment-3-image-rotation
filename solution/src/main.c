#include "file_io.h"
#include "picture.h"
#include "struct.h"


int main(int argc, char **argv) {
    if (argc < 3) {
        fprintf(stderr, "Invalid arguments.");
        return -1;
    }
    FILE *fread = NULL;
    FILE *fwrite = NULL;
    my_fopen(&fread, argv[1], "rb");
    my_fopen(&fwrite, argv[2], "wb");
    struct image *pic = NULL;
    enum read_status rstatus = from_bmp(fread, &pic);
    if (rstatus != READ_OK) {
        fprintf(stderr, "Error: %d", rstatus);
        return rstatus;
    }
    struct image *result = NULL;
    rotate(pic, &result);
    free_pic(pic);
    if (!result || !result->data) return -1;
    enum write_status wstatus = to_bmp(fwrite, result);
    if (wstatus != WRITE_OK) {
        fprintf(stderr, "Error: %d", wstatus);
        free_pic(result);
        return wstatus;
    }
    free_pic(result);
    my_fclose(fread);
    return 0;
}
