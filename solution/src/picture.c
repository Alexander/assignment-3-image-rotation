#include "picture.h"
#include "struct.h"

bool read_head(FILE *fin, struct bmp_header *header);

bool read_pic(FILE *fin, struct image *pic);

bool write_head(FILE *fout, struct bmp_header *header);

bool write_pic(FILE *fout, struct image *pic);

enum read_status from_bmp(FILE *fin, struct image **pic) {
    struct bmp_header header = {0};
    if (!read_head(fin, &header)) return READ_INVALID_HEADER;
    *pic = create_pic(header.biWidth, header.biHeight);
    if (!*pic || !(*pic)->data) {
        free_pic(*pic);
        return INIT_ERROR;
    }
    if (!read_pic(fin, *pic)) {
        free_pic(*pic);
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *fout, struct image *pic) {
    struct bmp_header *header = get_head_from_pic(pic);
    if (!write_head(fout, header)) {
        free_head(header);
        return WRITE_ERROR;
    }
    if (!write_pic(fout, pic)) {
        free_head(header);
        return WRITE_ERROR;
    }
    free_head(header);
    return WRITE_OK;
}

struct pixel get_px_from_pic(const struct image *pic, size_t index) {
    return pic->data[index];
}

void write_px_in_pic(struct image **pic, struct pixel px, size_t idx) {
    (*pic)->data[idx] = px;
}

void rotate(struct image *pic, struct image **new_pic) {
    *new_pic = create_pic(pic->height, pic->width);
    if (!*new_pic || !(*new_pic)->data) return;
    for (size_t width = 0; width < pic->width; width++) {
        for (size_t height = 0; height < pic->height; height++) {
            write_px_in_pic(new_pic, get_px_from_pic(pic, width + (pic->height - height - 1) * pic->width),
                            width * (pic->height) + height);
        }
    }
}


bool skip_padding(FILE *fin, struct image *const pic) {
    int64_t padding = get_padding(pic);
    if (fseek(fin, padding, SEEK_CUR)) return false;
    return true;
}

bool read_head(FILE *fin, struct bmp_header *header) {
    return fread(header, sizeof(struct bmp_header), 1, fin);
}

bool read_pic(FILE *fin, struct image *pic) {
    for (size_t height = 0; height < pic->height; height++) {
        for (size_t width = 0; width < pic->width; width++) {
            if (!fread((pic->data + (height * (pic->width) + width)), sizeof(struct pixel), 1, fin)) return false;
        }
        if (!skip_padding(fin, pic)) return false;
    }
    return true;
}

bool write_head(FILE *fout, struct bmp_header *const header) {
    return fwrite(header, sizeof(struct bmp_header), 1, fout);
}

bool write_padding(FILE *fout, struct image *const pic) {
    if (get_padding(pic) == 0) return true;
    uint8_t padding[3] = {0, 0, 0};
    return fwrite(&padding, sizeof(uint8_t), get_padding(pic), fout);
}

bool write_pic(FILE *fout, struct image *const pic) {
    for (size_t height = 0; height < pic->height; height++) {
        for (size_t width = 0; width < pic->width; width++) {
            if (!fwrite((pic->data + height * (pic->width) + width), sizeof(struct pixel), 1, fout)) return false;
        }
        if (!write_padding(fout, pic)) return false;
    }
    return true;
}
