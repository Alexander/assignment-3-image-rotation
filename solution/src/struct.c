#include "struct.h"

struct bmp_header *create_head(void) {
    return malloc(sizeof(struct bmp_header));
}

struct image *create_pic(uint64_t width, uint64_t height) {
    struct pixel *pixels = malloc(sizeof(struct pixel) * width * height);
    struct image *pic = malloc(sizeof(struct image));
    pic->height = height;
    pic->width = width;
    pic->data = pixels;
    return pic;
}

void free_head(struct bmp_header *header) {
    free(header);
}

void free_pic(struct image *pic) {
    free(pic->data);
    free(pic);
}

uint8_t get_padding(const struct image *pic) {
    uint8_t padding = 0;
    if (pic->width % 4 != 0) padding = 4 - (int64_t) ((pic->width * sizeof(struct pixel)) % 4);
    return padding;
}

size_t calc_file_size_from_pic(const struct image *const pic) {
    return pic->width * pic->height * sizeof(struct pixel) + get_padding(pic) * pic->height + sizeof(struct bmp_header);
}

size_t get_pic_size(const struct image *pic) {
    return pic->width + get_padding(pic) * pic->height;
}

struct bmp_header *get_head_from_pic(const struct image *pic) {
    struct bmp_header *header = NULL;
    while (!header) {
       header = create_head();
    }
    header->bfType = 0x4D42;
    header->bfileSize = calc_file_size_from_pic(pic);
    header->bfReserved = 0;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = 40;
    header->biWidth = pic->width;
    header->biHeight = pic->height;
    header->biPlanes = 1;
    header->biBitCount = 24;
    header->biCompression = 0;
    header->biSizeImage = get_pic_size(pic);
    header->biXPelsPerMeter = 0;
    header->biYPelsPerMeter = 0;
    header->biClrUsed = 0;
    header->biClrImportant = 0;
    return header;
}
