#include "file_io.h"

bool my_fopen(FILE **f, const char *fname, const char *mode) {
    *f = fopen(fname, mode);
    return (*f != NULL);
}

bool my_fclose(FILE *f) {
    return fclose(f) == 0;
}
