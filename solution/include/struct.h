#ifndef ASSIGNMENT_3_IMAGE_ROTATION_STRUCT_H
#define ASSIGNMENT_3_IMAGE_ROTATION_STRUCT_H

#include <inttypes.h>
#include <malloc.h>
#include <stdint.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct bmp_header *create_head(void);

struct image *create_pic(uint64_t width, uint64_t height);

void free_head(struct bmp_header *head);

void free_pic(struct image *pic);

uint8_t get_padding(const struct image *pic);

struct bmp_header *get_head_from_pic(const struct image *pic);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_STRUCT_H
