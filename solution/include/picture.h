#ifndef ASSIGNMENT_3_IMAGE_ROTATION_PICTURE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_PICTURE_H

#include "struct.h"
#include <stdbool.h>
#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    INIT_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status from_bmp(FILE *fin, struct image **pic);

enum write_status to_bmp(FILE *fout, struct image *pic);

struct pixel get_px_from_pic(const struct image *pic, size_t index);

void write_px_in_pic(struct image **pic, struct pixel px, size_t idx);

void rotate(struct image *pic, struct image **new_pic);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_PICTURE_H
