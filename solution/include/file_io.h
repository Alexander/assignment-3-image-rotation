#ifndef ASSIGNMENT_3_IMAGE_ROTATION_FILE_IO_H
#define ASSIGNMENT_3_IMAGE_ROTATION_FILE_IO_H

#include <stdbool.h>
#include <stdio.h>

bool my_fopen(FILE **f, const char *fname, const char *mode);

bool my_fclose(FILE* f);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_FILE_IO_H
